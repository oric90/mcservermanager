#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Docstring:"""

import requests
import argparse
import numpy
import os
from configparser import RawConfigParser


def parseInputKeywords():
    """Docstring:"""
    parser = argparse.ArgumentParser(usage="Manage a Minecraft Forge or Cauldron server installation")

    parser.add_argument('-mf', '--mod_urlFile', nargs='?', 
                            default=None, 
                            dest='mod_urlFile', 
                            help = 'File containing the Urls for downloading the mods, default: %(default)s')

    parser.add_argument('-mD', '--mod_destinationFolder', nargs='?', 
                            default='./downloads/', 
                            dest='mod_destinationFolder', 
                            help = 'Folder to store the downloaded mods, default: %(default)s')

    parserDict = vars(parser.parse_args())
    return parserDict


def downloadMods(urlFile, destinationFolder):
    """Docstring:"""
    modList = numpy.loadtxt(urlFile, dtype = str)
    if (not os.path.isdir(destinationFolder)):
        os.makedirs(destinationFolder)

    for modUrl in modList:
        modName = modUrl.split('/')[-1]
        print 'Downloading file ' + modName + '\n'  
        try:
            req = requests.get(modUrl, stream = True)
            if req.ok:
                with open(destinationFolder + modName, 'w') as modFile:
                    for chunk in req.iter_content(1024): 
                        if chunk: # filter out keep-alive new chunks
                            modFile.write(chunk)                
        except Exception, e:
            print str(e)


class ModLibrary(object):
    """Docstring:"""

class Mod(object):
    """Docstring:"""


if (__name__ == '__main__'):
    parserDict = parseInputKeywords()
    print parserDict
    if (parserDict['mod_urlFile']):
        downloadMods(parserDict['mod_urlFile'], parserDict['mod_destinationFolder'])



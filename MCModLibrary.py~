#!/usr/bin/python
# -*- coding: utf-8 -*-
"""DOCSTRING: """

#Imports
from configparser import RawConfigParser, DuplicateSectionError
import numpy
import os
from fnmatch import fnmatch
import IPython

class ModLibrary(object):
    """Docstring:"""
    __modList = []
    __library = None
    __downloadDir = None
    __configFile = None
    __libFile = None
    __modlib_exists = False

    def __init__(self, libFile = 'MC', downloadDirectory = './downloads/', force_new = False):
        """Docstring: If a library file already exists, use this and 
        extend/change if not forced to overwrite. Else create new"""
        self.__libFile = libFile
        self.__downloadDir = downloadDirectory

        searchDir = '/'.join(libFile.split('/')[:-1])
        if not searchDir:
            searchDir = '.'

        for fileName in os.listdir(searchDir + '/'):
            if (os.path.isfile(fileName) and fnmatch(fileName, '*.modlib')):
                self.__modlib_exists  = True
                self.__libFile = fileName
                break

        if (self.__modlib_exists and not force_new):
            self.edit()
        else:            
            try:
                os.remove(self.__libFile)
            except Exception, e:
                print 'No modlib file found or force_new enabled \n'
                print str(e)
            self.__modlib_exists = False
            self.edit()


    def edit(self):
        """Docstring:"""
        modLibrary = RawConfigParser()

        if (self.__modlib_exists):
            with open(self.__libFile, 'r') as libFile:
                modLibrary.readfp(libFile)

        self.generate_modList(self.__downloadDir)

        for mod in self.__modList:
            try:
                modLibrary.add_section(mod.name())
            except DuplicateSectionError:
                print 'Section ' + mod.name() + ' already exists \n'

            print 'Setting mod parameters \n'
            modLibrary.set(mod.name(), 'Version', mod.get_version())
            modLibrary.set(mod.name(), 'Type', mod.get_type())
            modLibrary.set(mod.name(), 'File', mod.get_file())
            modLibrary.set(mod.name(), 'Url', mod.get_url())
            modLibrary.set(mod.name(), 'Dependencies', mod.list_dependencies())
            
        self.__library = modLibrary


    def generate_modList(self, downloadDirectory):
        """Docstring:"""
        for fileName in os.listdir(downloadDirectory):
            if os.path.isfile(downloadDirectory + fileName):
                currentMod = Mod(fileName)
                currentMod.set_file(downloadDirectory + fileName)
                self.__modList.append(currentMod)


    def write(self, fileName):
        """Docstring:"""
        with open(fileName, 'wb') as configfile:
            self.__library.write(configfile)


    def get_modList(self):
        """Docstring:"""
        for mod in self.__modList:
            print mod.name()

    def get_urls(self, urlFile):
        """Docstring:"""
        modUrls = numpy.loadtxt(urlFile, dtype = str)    
        for libEntry in self.__library.sections():
            for modUrl in modUrls:
                if (fnmatch(modUrl, libEntry)):
                    print 'Found url ' + modUrl + 'for mod ' + libEntry + '\n'
                    self.__library.set(libEntry, 'Url', modUrl)                    


class Mod(object):
    """Docstring: A class containing information about a mod."""
    __name    = None
    __version = ''
    __url     = ''
    __depends = []
    __type    = None
    __file    = None

    def __init__(self, modName):
        """DOCSTRING: """
        self.generate(modName)

    def generate(self, modName):
        """DOCSTRING: """
        self.__name = modName

    def add_dependency(self, modName):
        """DOCSTRING: """
        self.__depends.extend(modName)

    def remove_dependency(self, modName):
        """DOCSTRING: """
        self.__depends.remove(modName)

    def list_dependencies(self):
        """DOCSTRING: """
        return self.__depends

    def name(self):
        """DOCSTRING: """
        return self.__name

    def set_type(self, type):
        """DOCSTRING: """
        self.__type = type

    def get_type(self):
        """DOCSTRING: """
        return self.__type

    def set_url(self, url):
        """DOCSTRING: """
        self.__url = url

    def get_url(self):
        """DOCSTRING: """
        return self.__url

    def set_version(self, version):
        """DOCSTRING: """
        self.__version = version
        
    def get_version(self):
        return self.__version

    def set_file(self, fileName):
        """DOCSTRING: """
        self.__file = fileName
        
    def get_file(self):
        return self.__file


if (__name__ == '__main__'):
    mod = Mod('test')
    print mod.__dict__.keys()
    modLib = ModLibrary()

    IPython.embed()
